const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt");
const auth = require("../auth")


// Check if the email already exist
/*
	Steps:
	1. Use mongoose query "find" method to find the duplicate emails.
	2. use the "then" mehtod to send back a response to the frontend application based on the result of fond method.

*/

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// if result is greater than 0, user is found/already exists.
		if (result.length > 0) {
			return true;
		}
		else{
			//No duplicated email found
			return false;
		}
	});
}

// User Registration
/*
	Steps:
	1. Create a new user object using the mongoose model and the information from the request body.
	2. Make sure that the password is ecnrypted.
	3. Save the new User database.

*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		//Syntax: bcrypt.hashSync(dataToBeEncrypted, salt)
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		}
		else{
			console.log(user);
			return true;
		}
	})
}

// User login
/*
	Steps:
	1. Check the database if the user's email is registered.
	2. Compare the password provided in the login form with the password stored in the database.
	3.Generate and return a JSON Web Token if the user is successfully login and return false if not

*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		// User does not exist
		if (result == null) {
			return false;
		}
		// User exist
		else{
			// Syntax: bcrypt.compareSync(data, encrypted)
			// reqBody.password & result.password
			// bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the passwords match the result.
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};
			}
			else{
				return "user does not exist";
			}
		}

	})
}



module.exports.userDetails = (data) =>{
	console.log(data);
	return User.findById(data.userId).then(result =>{
		result.password = "";

		return result;
	})
}

// Enroll user to a class
/*
	Steps:
	1. Find document in the database using the user's ID.
	2. Add the courseId to the user's enrollment array.
	3. Add the userId to the course's enrollees array.
	4. Update the document in the MongoDB Atlas database
*/

// Async await will be use in the enrolling the user because we will need to update 2 separate documnet (users, courses) when enrolling a user.
module.exports.enroll = async (data) => {						
	//true or false											//result
	console.log(data)
	let isUserUpdated = await User.findById(data.userId).then(user =>{
		//add the courseId in the user's enrollment array
		user.enrollments.push({courseId: data.courseId});

		// Saves the updated user information in the database
		return user.save().then((enrollment, error) =>{
			if (error) {
				return false;
			}
			else{
				return true;
			}
		})

	})
	console.log(isUserUpdated);
	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{
		console.log(course)
		course.enrollees.push({userId: data.userId})
		course.slots -= 1

		return course.save().then((enrollees, error) =>{
			if (error) {
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Minus the slots available by 1
	

	// Save the Updated course information in the database
	

	console.log(isCourseUpdated);

	// Condition that will check if the use and course documents have been updated.
	// User enrollment successful
	if (isUserUpdated && isCourseUpdated) {
		return true;
	}
	else{
		false;
	}


}

